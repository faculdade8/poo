

public class Dia {
	private byte dia;
	private byte mes;
	private short ano;
	
	private boolean ehBissexto(short ano) {
		return (ano % 400 == 0 || ((ano % 4 == 0) && (ano % 100 != 0)));
	}
	
	private byte getUltimoDia(byte mes, short ano) {
		byte ud [] = {0,31,28,31,30,31,30,31,31,30,31,30,31};
		
		if(mes == 2 && ehBissexto(ano)) {
			return 29;
		}
		return ud[mes];
	}

	public Dia() {
		setAno((byte)1);
		setMes((byte)1);
		setDia((byte)1);
	}
	public Dia(byte dia, byte mes, short ano) {
		this();
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
	public byte getDia() {
		return dia;
	}
	public void setDia(byte dia) {
		byte ultimoDia = getUltimoDia(mes, ano);
		if(dia >= 1 && dia <= ultimoDia) {
			this.dia = dia;
		}else {
			byte a = (byte) (dia % ultimoDia);
			incrementaMes(a);
		}

	}
	public byte getMes() {
		return mes;
	}
	public void setMes(byte mes) {
		if(mes >= 1 && mes <= 12) {
			this.mes = mes;
		}
	}
	public void incrementaMes(byte mes) {
		if(mes >= 1 && mes <= 12) {
			this.mes = mes;
			mes++;
		}
		
		incrementaAno();
	}
	public short getAno() {
		return ano;
	}
	public void setAno(short ano) {
		if(ano >=1 && ano <= 4000) {
			this.ano = ano;
		}
	}
	/*public void incrementaDia(int dia) {
		byte d = (byte)(dia);
		if(d == getUltimoDia(mes, ano)) {
			this.dia = d;
		}
		incrementaMes();
	}
	public void incrementaMes() {
		byte m = (byte)(mes);
		if(m >= 1 && m <= 12) {
			this.mes = m;
		}
		incrementaAno();
	}*/
	public void incrementaAno() {
		short a = (short)(ano + 1);
	}
	
	
	public void print() {
		System.out.println( getDia() + "/" + getMes() + "/" + getAno());
	}
}
