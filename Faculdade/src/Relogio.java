
public class Relogio {
	private IHorario hms;
	private Data dma;
	
	public Relogio(IHorario hms, Data dma) {
		this.hms = new Horario(hms);
		this.dma = dma;
	}
	public void tictac() {
		hms.adicionarSegundo();
		if(hms.ehPrimeiroHorario()) {
			dma.incrementaDia();
		}
	}
	
	@Override
	public String toString() {
		return dma + " " + hms;
	}
	@SuppressWarnings("unlikely-arg-type")
	public boolean equals(Object obj) {
		if(this == obj) {
			return true;
		}
		if(obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		Relogio r = (Relogio) obj;
		
		return  dma.equals(r) && hms.equals(r);
	}
	public boolean menor(Relogio re) {
		if(re.dma.menor(dma) && re.hms.menor(hms)) {
			return true;
		}
		return false;
	}
	public boolean maior(Relogio re) {
		if(re.dma.maior(dma) && re.hms.maior(hms)) {
			return true;
		}
		return false;
	}
	public boolean menorIgual(Relogio re, Object obj) {
		if(menor(re) == true || equals(obj) == true) {
			return true;
		}
		return false;
	}
	public boolean maiorIgual(Relogio re, Object obj) {
		if(maior(re) == true || equals(obj) == true) {
			return true;
		}
		return false;
	}
}
